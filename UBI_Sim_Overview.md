## UBI Sim Overview


### Previous Work
* Fun, readable description of a [python monte-carlo sim](https://www.chrisstucchio.com/blog/2013/basic_income_vs_basic_job.html) comparing UBI to Guaranteed Jobs, by Chris Stuccio [2013].
* [UBI examples using Policy Simulation Library](https://github.com/PSLmodels/UBI-examples), published by OSPC.
  * Note that OSPC is affiliated with AEI, often considered to be a "conservative think tank".   
  * [OSPC](http://apps.ospc.org/) = Open Source Policy Center.  [Source for Policy Brain](https://github.com/ospc-org/ospc.org)
  * [Catalog of PSL Models](https://www.pslmodels.org/)
* Unverified results from the <b>closed-source</b> Penn Wharton [Budget Model Simulation of UBI](http://budgetmodel.wharton.upenn.edu/issues/2018/3/29/options-for-universal-basic-income-dynamic-modeling)
  * Key prediction from the PWBM team is that UBI reduces incentive to work, which we wish to question in detail.
  * What are UBI effects on:
    * Worker mobility 
    * Worker continuing education
    * Small business startup/success/failure rates
* [Review of Tax Modeling Methodologies](https://eml.berkeley.edu/~auerbach/dynamic%20scoring.pdf) from National Tax Journal 
* Methods combining macro and micro-simulation in econometrics
    * [TDB Macro+Micro sim method](https://www.pep-net.org/microsimulation-distributive-analysis) :  Tiberti, L., M. Cicowiez and J. Cockburn (2017), "A top-down with behaviour (TDB) microsimulation toolkit for distributive analysis", PEP
* Agent models of Money Supply
  * [Simulating the Fractional Reserve Banking](https://ccl.northwestern.edu/2016/monett.pdf)  using
Agent-based Modelling with NetLogo (2016) by Monett and Navarro-Barrientos, Berlin School of Econ.

  * [Cash Flow Model](http://ccl.northwestern.edu/netlogo/models/CashFlow) - simple open-source model in NetLogo.

* Econ Model Concepts
  * https://en.wikipedia.org/wiki/Convex_preferences

