;; each turn, adults must divide their time between activities:  Work, Study, Shop, Rest.
;; Cash is earned during Work, at cost of Qi (and time).  Skill determines our productivity and pay.
;; Eating and chi restoration occurs during rest.
;; The fractions of time spent on these activities are treated as 4 slowly-changing variables,
;; with these constraints:
;; A) Total time spent per turn must sum to 1.0
;; B) tfrac_shop_full = tfrac_shop + tfrac_trvl_shop
;; C) tfrac_work_full = tfrac_work + tfrac_trvl_work
;; tfrac_trvl_* is proportional to distance to shop and work.  Say max possible tfrac for each is 0.2
;; tfrac_work and tfrac_shop are based on immediate physical and economic necessity.
;; Remaining time is then split between study and rest.
;; tfrac_rest has a minimum amount of 0.1.
;; Let farmEfficiency represent quality of farm machinery, which requires ongoing cash investment.
;; farm demand is for sufficient workerSkill * workerTime * farmEfficiency to grow enough rice to satisfy grocer demand.
;; If avgSkill of the available workforce goes up, we expect workerTime to decrease.
;; Then either payPerTime must increase, or workers will need to find additional income, or get by with less.
;; If farms lower their prices to grocers, and grocers follow suit, then worker COL goes down.
;;
;; Fruits of increased worker skill and improved farm efficiency may go to:
;;   farm owners (govt may try to tax some of these gains away, and redistrib to workers)
;;   farm workers
;;   farm customers (= grocers, who have customers that overlap with workers, at other end of supply-chain)
;;
;;
;; a turn is 1.0 day
;; chi and skill both normally range from 0.1 to 0.9
;; each adult eats 1.0 kg of rice per day.
;; avg adult farm wages are 100Y per day.
;; Thus steady-state aggregate rice output needs to be above 1.0 x total-adults (1 + spoilage) = aggGrownPerDay.
;; Suppose farms expect to pay adults an average wage of 100Y in exchange for this productivity,
;; so total wages paid by farms = num-adults * 100Y.

;; where spoilage is the amount spoiled between growing and eating.
;;
;; skill rate decays faster when > 0.5
;;
;; choice variables
;; After subtracting away involuntary travel time and shopping-time costs, there is a remaining
;; time-choice pie to divide among:  work, study, rest
;; where:     work turns chi into cash (modulated by skill)
;;            study turns chi into skill = a form of investing in future QOL
;;            rest restores chi (using food bought with cash)
;; high QOL = sustained high chi and balance of work/study/rest
;; happiness ~ chi


;; The time cost of Work and Shop has a lower bound proportional
;; to distance from the linked Work and Shop locations
;; (If a turn was like a day, then it would be more realistic
;; Time spent on movement depends on whether or not adult is working
;; + shopping that day.
;; proportional to distance from work and grocer.  However if we
;; allow adults to only work

breed [ adults adult ]
breed [ farms farm ]
breed [ grocers grocer ]
breed [ houses house ]
breed [ trucks truck ]
breed [ banks bank ]
breed [ fedgovs fedgov ]

directed-link-breed [work-ats works-at]

;; Units:
;; Cash  : Yen integer
;; Food  : Rice float
;; Chi   : Qi  float
;; Skill : Kata float
;; Effort = Chi * Skill

;; Rice is infinitely divisible, but Cash acts like an integer.
;; So we store price as rice-per-yen, and invert as needed.

adults-own [ chi food cash skill eat_RpT digest_QpR  fracWork fracShop fracStudy fracRest fracMove]
farms-own [ food cash sell_RpY  growRate_RpE  invrsPay_RpY]
grocers-own [ food cash sell_RpY ]

patches-own [ patchState ]

;; "You can think of global variables as belonging to the observer. "
;; Note: Some other globals are auto-declared by input GUI widgets.

globals [ ubi_YpT ]

__includes [ "price_funcs.nls" "deps_mod.nls" ]

to setup
  clear-all
  set-default-shape adults "face neutral"
  set-default-shape houses "house"
  set-default-shape farms "flower"
  set-default-shape grocers "building store"

  ;; set sldr_ubi_YpT 14

  create-adults init_adults [
    set eat_RpT 0.01
    set digest_QpR 0.2
    set skill 0.5
    ;; color half the turtles red,
    ;; the other half yellow
    set color one-of [yellow red]
    setxy random-xcor random-ycor
    set size 2.5

  ]
  create-houses 11 [
    set color one-of [violet orange]
    setxy random-xcor random-ycor
    set size 4
  ]
  create-farms 3 [
    set sell_RpY 0.2
    set growRate_RpE 0.3
    set invrsPay_RpY 6.5 ;; Workers must grow 6.5 rice to earn one Yen of pay.
    setxy random-xcor random-ycor
    set size 10
  ]
  create-grocers 5 [
    set sell_RpY 0.05
    setxy random-xcor random-ycor
    set size 6
  ]

  ask adults [
    create-works-at-to one-of farms [
      set color yellow
    ]
  ]

  setup-deps 45

  ask patches [
    set pcolor green      ;; color the patches green
  ]
  reset-ticks
end

to go
  reallyWork
  reallyTrade
  jumpAround
  tick
end

to be-weird
  ask turtles [ draw-polygon-chgd 8 1]
end

to jumpAround
  ask adults [
    rt random 90
    lt random 90
    fd 0.01
    ;; if you're a red turtle, turn your patch black
    if color = red
      [ set pcolor black ]
    ;; if you're a yellow turtle, turn your patch magenta
    if color = yellow
      [ set pcolor magenta ]
  ]

end
;; Each adult works at one business of each type (farm, grocery, ...)
to reallyWork
  set ubi_YpT sldr_ubi_YpT
  ask adults [
    receiveCash ubi_YpT
    let someFarm one-of farms
    workAtFarm someFarm
    eatFood
    studySkill
  ]
end
;; Converge towards:
;; All grocers buy from all farms, splitting cost of transport

 to reallyTrade
  let buyingGrocer one-of grocers
  let sellingFarm one-of farms
  ask sellingFarm [
    sellFood buyingGrocer 5
  ]
  let buyingAdult one-of adults
  let sellingGrocer one-of grocers
  ask sellingGrocer [
    sellFood buyingAdult 1
  ]
end

to eatFood ;; applies to adults, juniors, seniors
  let riceEaten eat_RpT
  let qiGained (riceEaten * digest_QpR)
  receiveChi qiGained
  receiveFood (-1 * riceEaten)
end
to sellFood [buyer yenAmt] ;; Applies to Farms and Grocers
  let riceAmt (sell_RpY * yenAmt)
  tradeFoodForCash buyer self riceAmt yenAmt
end

to tradeFoodForCash [buyer seller riceAmt yenAmt]
  ask seller [
    receiveCash yenAmt
    receiveFood (-1 * riceAmt)
  ]
  ask buyer [
    receiveCash (-1 * yenAmt)
    receiveFood riceAmt
  ]
end
to recordFarmWork [adlt usedQiAmt grownRiceAmt payYenAmt] ;; Applies to Farms
  receiveCash (-1 * payYenAmt)
  receiveFood grownRiceAmt
  ask adlt [
    receiveCash payYenAmt
    receiveChi (-1 * usedQiAmt)
  ]
end
to workAtFarm [frm]  ;; Applies to Adults
  let chiUsed 0.1
  let grthEffort chiUsed * skill
  let grt [growRate_RpE] of frm
  let grwnR (grt * grthEffort)
  let prt 1.0 / ([invrsPay_RpY] of frm)
  let pay prt * grwnR
  ask frm [
    recordFarmWork myself chiUsed grwnR pay
  ]
end

to receiveChi [qiAmt]
  set chi chi + qiAmt
end
to receiveCash [yenAmt]
  set cash cash + yenAmt
end
to receiveFood [riceAmt]
  set food food + riceAmt
end
to receiveSkill [kataAmt]
  set skill skill + kataAmt
end

to studySkill
  receiveSkill 0.01
end

to-report avgCash [agts]
  report mean [cash] of agts
end
to-report avgFood [agts]
  report mean [food] of agts
end
to-report avgChi [agts]
  report mean [chi] of agts
end
to-report avgSkill [agts]
  report mean [skill] of agts
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
614
415
-1
-1
12.0
1
10
1
1
1
0
1
1
1
-16
16
-16
16
0
0
1
ticks
30.0

BUTTON
320
425
397
458
(re-)gen
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
425
425
512
458
run/pause
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
369
466
457
499
octoWeird
be-weird
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
645
445
705
490
NIL
avgKidsPerAdult
17
1
11

SLIDER
15
55
187
88
sldr_ubi_YpT
sldr_ubi_YpT
0
100
0.0
1
1
NIL
HORIZONTAL

PLOT
630
10
945
195
avg_income_and_costs
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"avg_inc" 1.0 0 -4079321 true "" "plot avgCash adults"
"pen-1" 1.0 0 -10402772 true "" "plot avgCash grocers"

MONITOR
645
215
740
260
adult avg cash (Y)
avgCash adults
17
1
11

CHOOSER
20
250
158
295
chz_wrkRlz
chz_wrkRlz
"wr A" "wr B"
1

SLIDER
15
10
187
43
init_adults
init_adults
0
100
39.0
1
1
NIL
HORIZONTAL

MONITOR
645
270
740
315
Adult avg food  (Rice)
avgFood adults
17
1
11

MONITOR
750
215
842
260
farm avg cash
avgCash farms
17
1
11

MONITOR
750
270
842
315
farm avg food
avgFood farms
17
1
11

MONITOR
850
215
950
260
grocer avg cash
avgCash grocers
17
1
11

MONITOR
845
270
947
315
grocer avg food
avgFood grocers
17
1
11

MONITOR
645
325
727
370
adult avg chi
avgChi adults
17
1
11

MONITOR
645
380
732
425
adult avg skill
avgSkill adults
17
1
11

SLIDER
30
100
202
133
base_farm_grow_rate
base_farm_grow_rate
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
15
150
197
183
base_grocer_stock_rate
base_grocer_stock_rate
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
25
205
197
238
base_adult_eat_rate
base_adult_eat_rate
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
15
310
187
343
skill_cost_in_chi
skill_cost_in_chi
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
25
360
197
393
chi_cost_in_food
chi_cost_in_food
0
100
50.0
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

building store
false
0
Rectangle -7500403 true true 30 45 45 240
Rectangle -16777216 false false 30 45 45 165
Rectangle -7500403 true true 15 165 285 255
Rectangle -16777216 true false 120 195 180 255
Line -7500403 true 150 195 150 255
Rectangle -16777216 true false 30 180 105 240
Rectangle -16777216 true false 195 180 270 240
Line -16777216 false 0 165 300 165
Polygon -7500403 true true 0 165 45 135 60 90 240 90 255 135 300 165
Rectangle -7500403 true true 0 0 75 45
Rectangle -16777216 false false 0 0 75 45

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="3" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="500"/>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="init_adults">
      <value value="39"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sldr_ubi_YpT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="chz_wrkRlz">
      <value value="&quot;wr B&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
